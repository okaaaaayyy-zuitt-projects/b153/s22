/*
User document: 
{
	_id: <ObjecyId1>,
	user: "user123"
	email: "email@email.com"
	password: "p@ssword"
	fullName: "Juan dela Cruz"
	shippingAddress: "143 St. Batangas City, Batangas, Philippines"
	contact: "0000-111-2222"
}

Product Document:
{
	_id: <ObjecyId2>,
	sku: "abcde12345",
	productName: "Product One"
	price: 100
	weight: "0.5 kg"
	description: "Sample Product Description"
	category: "Category 1"
	creationDate: 1-1-2022
	stock: 12
}

Order Document:
{
	_id: <ObjecyId3>,
	customerId: "customer12345"
	amount: 1000
	shippingAddress: "143 St. Batangas City, Batangas, Philippines"
	orderDate: 2-4-2022
	orderDetails:
		{
			_id: <ObjecyId4> ,
			orderId: "order12345"
			productId: "product12345"
			price: 9
			sku: "abcde12345",
			quantity: 1
		}
}



*/